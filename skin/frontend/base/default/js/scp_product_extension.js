/*
    Some of these override earlier varien/product.js methods, therefore
    varien/product.js must have been included prior to this file.
*/

Product.Config.prototype.getMatchingSimpleProduct = function(){
    var inScopeProductIds = this.getInScopeProductIds();
    if ((typeof inScopeProductIds != 'undefined') && (inScopeProductIds.length == 1)) {
        return inScopeProductIds[0];
    }
    return false;
};

/*
    Find products which are within consideration based on user's selection of
    config options so far
    Returns a normal array containing product ids
    allowedProducts is a normal numeric array containing product ids.
    childProducts is a hash keyed on product id
    optionalAllowedProducts lets you pass a set of products to restrict by,
    in addition to just using the ones already selected by the user
*/
Product.Config.prototype.getInScopeProductIds = function(optionalAllowedProducts) {

    var childProducts = this.config.childProducts;
    var allowedProducts = [];

    if ((typeof optionalAllowedProducts != 'undefined') && (optionalAllowedProducts.length > 0)) {
       // alert("starting with: " + optionalAllowedProducts.inspect());
        allowedProducts = optionalAllowedProducts;
    }

    for(var s=0, len=this.settings.length-1; s<=len; s++) {
        if (this.settings[s].selectedIndex <= 0){
            break;
        }
        var selected = this.settings[s].options[this.settings[s].selectedIndex];
        if (s==0 && allowedProducts.length == 0){
            allowedProducts = selected.config.allowedProducts;
        } else {
           // alert("merging: " + allowedProducts.inspect() + " with: " + selected.config.allowedProducts.inspect());
            allowedProducts = allowedProducts.intersect(selected.config.allowedProducts).uniq();
           // alert("to give: " + allowedProducts.inspect());
        }
    }

    //If we can't find any products (because nothing's been selected most likely)
    //then just use all product ids.
    if ((typeof allowedProducts == 'undefined') || (allowedProducts.length == 0)) {
        productIds = Object.keys(childProducts);
    } else {
        productIds = allowedProducts;
    }
    return productIds;
};


Product.Config.prototype.getProductIdOfCheapestProductInScope = function(priceType, optionalAllowedProducts) {

    var childProducts = this.config.childProducts;
    var productIds = this.getInScopeProductIds(optionalAllowedProducts);

    var minPrice = Infinity;
    var lowestPricedProdId = false;

    //Get lowest price from product ids.
    for (var x=0, len=productIds.length; x<len; ++x) {
        var thisPrice = Number(childProducts[productIds[x]][priceType]);
        if (thisPrice < minPrice) {
            minPrice = thisPrice;
            lowestPricedProdId = productIds[x];
        }
    }
    return lowestPricedProdId;
};


Product.Config.prototype.getProductIdOfMostExpensiveProductInScope = function(priceType, optionalAllowedProducts) {

    var childProducts = this.config.childProducts;
    var productIds = this.getInScopeProductIds(optionalAllowedProducts);

    var maxPrice = 0;
    var highestPricedProdId = false;

    //Get highest price from product ids.
    for (var x=0, len=productIds.length; x<len; ++x) {
        var thisPrice = Number(childProducts[productIds[x]][priceType]);
        if (thisPrice >= maxPrice) {
            maxPrice = thisPrice;
            highestPricedProdId = productIds[x];
        }
    }
    return highestPricedProdId;
};



Product.Config.prototype.updateFormProductId = function(productId){
    if (!productId) {
        return false;
    }
    var currentAction = $('product_addtocart_form').action;
    newcurrentAction = currentAction.sub(/product\/\d+\//, 'product/' + productId + '/');
    $('product_addtocart_form').action = newcurrentAction;
    $('product_addtocart_form').product.value = productId;
};


Product.Config.prototype.addParentProductIdToCartForm = function(parentProductId) {
    if (typeof $('product_addtocart_form').cpid != 'undefined') {
        return; //don't create it if we have one..
    }
    var el = document.createElement("input");
    el.type = "hidden";
    el.name = "cpid";
    el.value = parentProductId.toString();
    $('product_addtocart_form').appendChild(el);
};



Product.OptionsPrice.prototype.updateSpecialPriceDisplay = function(price, finalPrice) {

    var prodForm = $('product_addtocart_form');

    var specialPriceBox = prodForm.select('p.special-price');
    var oldPricePriceBox = prodForm.select('p.old-price, p.was-old-price');
    var magentopriceLabel = prodForm.select('span.price-label');

    if (price == finalPrice) {
        specialPriceBox.each(function(x) {x.hide();});
        magentopriceLabel.each(function(x) {x.hide();});
        oldPricePriceBox.each(function(x) {
            x.removeClassName('old-price');
            x.addClassName('was-old-price');
        });
    }else{
        specialPriceBox.each(function(x) {x.show();});
        magentopriceLabel.each(function(x) {x.show();});
        oldPricePriceBox.each(function(x) {
            x.removeClassName('was-old-price');
            x.addClassName('old-price');
        });
    }
};

//This triggers reload of price and other elements that can change
//once all options are selected
Product.Config.prototype.reloadPrice = function() {
    var childProductId = this.getMatchingSimpleProduct();
    var childProducts = this.config.childProducts;
    var oldchildProductId = localStorage.getItem('oldchildProductId');
    var usingZoomer = false;
    if(this.config.imageZoomer){
        usingZoomer = true;
    }

    if(childProductId){
        if ((oldchildProductId == null) || (oldchildProductId != childProductId)) {
            localStorage.setItem('oldchildProductId', childProductId);
            localStorage.setItem('noAjaxaction', 'disattivo');
            var price = childProducts[childProductId]["price"];
            var finalPrice = childProducts[childProductId]["finalPrice"];
            optionsPrice.productPrice = finalPrice;
            optionsPrice.productOldPrice = price;
            optionsPrice.reload();
            optionsPrice.reloadPriceLabels(true);
            optionsPrice.updateSpecialPriceDisplay(price, finalPrice);
            this.updateProductShortDescription(childProductId);
            this.updateProductDescription(childProductId);
            this.updateProductName(childProductId);
            this.updateProductAttributes(childProductId);
            this.updateFormProductId(childProductId);
            this.addParentProductIdToCartForm(this.config.productId);
            this.showCustomOptionsBlock(childProductId, this.config.productId);
            if (usingZoomer) {
                this.showFullImageDiv(childProductId, this.config.productId);
            }else{
                this.updateProductImage(childProductId);
            }
        } else {
            localStorage.setItem('noAjaxaction', 'attivo');
        }
        
    } else {
        var cheapestPid = this.getProductIdOfCheapestProductInScope("finalPrice");
        //var mostExpensivePid = this.getProductIdOfMostExpensiveProductInScope("finalPrice");
        var price = childProducts[cheapestPid]["price"];
        var finalPrice = childProducts[cheapestPid]["finalPrice"];
        optionsPrice.productPrice = finalPrice;
        optionsPrice.productOldPrice = price;
        optionsPrice.reload();
        optionsPrice.reloadPriceLabels(false);
        optionsPrice.updateSpecialPriceDisplay(price, finalPrice);
        this.updateProductShortDescription(false);
        this.updateProductDescription(false);
        this.updateProductName(false);
        this.updateProductAttributes(false);
        this.showCustomOptionsBlock(false, false);
        if (usingZoomer) {
            this.showFullImageDiv(false, false);
        }else{
            this.updateProductImage(false);
        }
    }
};



Product.Config.prototype.updateProductImage = function(productId) {
    var imageUrl = this.config.imageUrl;
    if(productId && this.config.childProducts[productId].imageUrl) {
        imageUrl = this.config.childProducts[productId].imageUrl;
    }

    if (!imageUrl) {
        return;
    }

    if($('image')) {
        $('image').src = imageUrl;
    } else {
        $$('#product_addtocart_form p.product-image img').each(function(el) {
            var dims = el.getDimensions();
            el.src = imageUrl;
            el.width = dims.width;
            el.height = dims.height;
        });
    }
};

Product.Config.prototype.updateProductName = function(productId) {
    var productName = this.config.productName;
    if (productId && this.config.childProducts[productId].productName) {
        productName = this.config.childProducts[productId].productName;
    }
    $$('#product_addtocart_form div.product-name h1').each(function(el) {
        el.innerHTML = productName;
    });
};

Product.Config.prototype.updateProductShortDescription = function(productId) {
    var shortDescription = this.config.shortDescription;
    if (productId && this.config.childProducts[productId].shortDescription) {
        shortDescription = this.config.childProducts[productId].shortDescription;
    }
    $$('#product_addtocart_form div.short-description div.std').each(function(el) {
        el.innerHTML = shortDescription;
    });
};

Product.Config.prototype.updateProductDescription = function(productId) {
    var description = this.config.description;
    if (productId && this.config.childProducts[productId].description) {
        description = this.config.childProducts[productId].description;
    }
    $$('div.box-description div.std').each(function(el) {
        el.innerHTML = description;
    });
};

Product.Config.prototype.updateProductAttributes = function(productId) {
    var productAttributes = this.config.productAttributes;
    if (productId && this.config.childProducts[productId].productAttributes) {
        productAttributes = this.config.childProducts[productId].productAttributes;
    }
    //If config product doesn't already have an additional information section,
    //it won't be shown for associated product either. It's too hard to work out
    //where to place it given that different themes use very different html here
    $$('div.product-collateral div.box-additional').each(function(el) {
        el.innerHTML = productAttributes;
        decorateTable('product-attribute-specs-table');
    });
};

Product.Config.prototype.showCustomOptionsBlock = function(productId, parentId) {
    var coUrl = this.config.ajaxBaseUrl + "co/?id=" + productId + '&pid=' + parentId;
    var prodForm = $('product_addtocart_form');

   if ($('SCPcustomOptionsDiv')==null) {
      return;
   }

    //Effect.Fade('SCPcustomOptionsDiv', { duration: 0.5, from: 1, to: 0.5 });
    if(productId) {
        //Uncomment the line below if you want an ajax loader to appear while any custom
        //options are being loaded.
        
        //$$('span.scp-please-wait').each(function(el) {el.show()});
        var noAjaxaction = localStorage.getItem('noAjaxaction');
        if (noAjaxaction == 'disattivo') {
            jQuery(".dettagli_prodotto_personalizzato").fadeOut(100);
            jQuery(".quantita_prezzo").fadeOut(100);
            //jQuery(".misure-contenitor").fadeOut(100);
            new Ajax.Request(this.config.AuaBaseUrl + 'simpleproductbasicdata/index/getPrintAttributes', {
                method: 'POST',
                parameters: { product_id: productId },
                requestHeaders: { Accept: 'application/json' },
                onSuccess:function(transport) {
                    var response = transport.responseText.evalJSON(true);
                    /* Now use response.image, see below */
                    //console.log(response);
                    var unita_misura = response.unita_misura;
                    console.log("unità di misura del prodotto: " + unita_misura);
                    jQuery("td.nome_prodotto_personalizzato").html(response.nome);
                    jQuery("td.materiale_prodotto_personalizzato").html(response.materiale);
                    jQuery("td.grammatura_prodotto_personalizzato").html(response.grammatura);
                    jQuery("td.manico_prodotto_personalizzato").html(response.manico);
                    jQuery("td.stampa_prodotto_personalizzato").html(response.stampa);
                    jQuery("td.area_di_stampa_prodotto_personalizzato").html(response.area_stampa);
                    jQuery("td.personalizzazione_prodotto_personalizzato").html(response.personalizzazione);
                    jQuery("div.price-box span.regular-price").html(response.prezzo_base)
                    jQuery(".dettagli_prodotto_personalizzato").fadeIn(1000);
                    jQuery("div.pezzi_per_confezione").text(response.pezzi_confezione);
                    jQuery("div.pezzi_per_confezione").hide();
                    if ( (unita_misura != null) && (unita_misura != "null") && (unita_misura != undefined) && (unita_misura != 'undefined') && (unita_misura != '') ) {
                        jQuery("table.quantita_prezzo thead tr th.unita_misura").text(response.unita_misura);
                        jQuery(".preventivo-pezzi").text(response.unita_misura);
                    }
                    //jQuery(".misure-contenitor").fadeIn(1000);
                    var i = 0;
                    jQuery("table.quantita_prezzo td.q_prodotto_personalizzato").each(function () {
                        var qtyp = jQuery(this).attr('data-qty');
                        jQuery(this).text(response.pezzi_confezione*qtyp);
                        if (i == 0) {
                            console.log("Valorizzo il campo quantità del preventivo con questo valore: " + response.pezzi_confezione*qtyp);
                            jQuery("td.pz_confezione").html(response.pezzi_confezione*qtyp);
                            i++;
                        }
                    });
                }.bind(this)
            });
            new Ajax.Request(this.config.AuaBaseUrl + 'simpleproductbasicdata/index/getSimpleProductGuidaAlleMisure', {
                method: 'POST',
                parameters: { product_id: productId },
                requestHeaders: { Accept: 'application/json' },
                onSuccess:function(transport) {
                    var response = transport.responseText.evalJSON(true);
                    //if (response.guida_alle_misure != null) {
                        //jQuery("img.prodotto_personalizzato_guida_alle_misure").attr("src", response.guida_alle_misure);
                    //}
                    if (response.area_di_stampa != null) {
                        jQuery("img.guida_alle_misure").attr("src", response.area_di_stampa);
                         //jQuery("img.img_guida_alle_misure_button").remove();
                        //jQuery("div#guida-alle-misure-container a.link_modale_guida_alle_misure").prepend('<img class="img_guida_alle_misure_button" src="'+response.area_di_stampa+'" />');
                        //jQuery("img.img_guida_alle_misure_button").attr("src", response.area_di_stampa);
                    }
                }.bind(this)
            });

            //prodForm.getElements().each(function(el) {el.disable()});
            new Ajax.Updater('SCPcustomOptionsDiv', coUrl, {
            method: 'get',
            evalScripts: true,
            onComplete: function() {
                console.group("Tabella Prezzi");
                $$('span.scp-please-wait').each(function(el) {el.hide()});
                Effect.Fade('SCPcustomOptionsDiv', { duration: 0.5, from: 0.5, to: 1 });
                //prodForm.getElements().each(function(el) {el.enable()});   
                console.info("Pulisco la tabella dei prezzi");
                jQuery("table.quantita_prezzo tbody").html("");      
                var pezzi_per_confezione = jQuery("div.pezzi_per_confezione").text();   
                
                jQuery("table.quantita_prezzo tbody").append('<tr>' +
                    '<td class="q_prodotto_personalizzato" data-qty="1">' +
                    (pezzi_per_confezione*1)+
                    '</td>' +
                    ' <td class="p_prodotto_personalizzato" data-qty="1"> ' +
                    '<span class="p_price" data-original-p-price="">'+ jQuery("div.price-box span.regular-price").text() +'</span> ' +
                    '</td> ' +
                '</tr>');


                jQuery("li.tier-price .tier-qty span").each(function () {
                    console.info("Trovato un tier price per la quantità: " + jQuery(this).text());
                    
                    var prezzo_offerta = jQuery(this).parent().parent().find(".price").text();
                    console.info("Prezzo offerta: " + prezzo_offerta);
                    console.info("Ogni confezione ha: " + pezzi_per_confezione + " pezzi.");
                    jQuery("table.quantita_prezzo tbody").append('<tr>' +
                    '<td class="q_prodotto_personalizzato" data-qty="'+jQuery(this).text()+'">' +
                    (pezzi_per_confezione*jQuery(this).text())+
                    '</td>' +
                    ' <td class="p_prodotto_personalizzato" data-qty="'+jQuery(this).text()+'"> ' +
                    '<span class="p_price" data-original-p-price="">'+ prezzo_offerta +'</span> ' +
                    '</td> ' +
                    '</tr>');
                }).promise().done( function(){ 
                    jQuery(".quantita_prezzo").fadeIn(100).promise().done( function() { 
                        jQuery("table.quantita_prezzo tbody tr td[data-qty='1']").trigger("click");
                     });
                    /*jQuery("li.tier-price .price").each(function () {
                        if (jQuery("td.p_prodotto_personalizzato[data-qty='"+dataqty+"'] span.p_price").length > 0) {
                            jQuery("td.p_prodotto_personalizzato[data-qty='"+dataqty+"'] span.p_price").parent().parent().show();    
                            jQuery("td.p_prodotto_personalizzato[data-qty='"+dataqty+"'] span.p_price").html(jQuery(this).text());
                        }
                        dataqty++;
                    }).promise().done( function(){ 
                        jQuery("td.p_prodotto_personalizzato span.p_price:empty").each(function () {
                            jQuery(this).parent().parent().hide();
                        }) 
                        
                        if ((jQuery('.q_prodotto_personalizzato.qp_prodotto_selezionato').html() != ' ') && (jQuery('.q_prodotto_personalizzato.qp_prodotto_selezionato').html() != '')) {
                            //console.log("Lancio la funzione ConfigurableGeneratePreview linea 359");
                            //console.log("Quantità dei prodotti che andrò a inserire: " + jQuery('.q_prodotto_personalizzato.qp_prodotto_selezionato').html());
                            ConfigurableGeneratePreview();
                        }
                    } );*/
                });
                
                console.groupEnd();
            }
            });
        }
    } else {
        $('SCPcustomOptionsDiv').innerHTML = '';
        try{window.opConfig = new Product.Options([]);} catch(e){}
    }
};


Product.Config.prototype.showFullImageDiv = function(productId, parentId) {
    var imgUrl = this.config.ajaxBaseUrl + "image/?id=" + productId + '&pid=' + parentId;
    var prodForm = $('product_addtocart_form');
    var destElement = false;
    var defaultZoomer = this.config.imageZoomer;

    prodForm.select('div.product-img-box').each(function(el) {
        destElement = el;
    });

    //TODO: This is needed to reinitialise Product.Zoom correctly,
    //but there's still a race condition (in the onComplete below) which can break it
    try {product_zoom.draggable.destroy();} catch(x) {}

    if(productId) {
        new Ajax.Updater(destElement, imgUrl, {
            method: 'get',
            evalScripts: false,
            onComplete: function() {
                //Product.Zoom needs the *image* (not just the html source from the ajax)
                //to have loaded before it works, hence image object and onload handler
                if ($('image')){
                    var imgObj = new Image();
                    imgObj.onload = function() {product_zoom = new Product.Zoom('image', 'track', 'handle', 'zoom_in', 'zoom_out', 'track_hint'); };
                    imgObj.src = $('image').src;
                } else {
                    destElement.innerHTML = defaultZoomer;
                    product_zoom = new Product.Zoom('image', 'track', 'handle', 'zoom_in', 'zoom_out', 'track_hint')
                }
          }
        });
    } else {
        destElement.innerHTML = defaultZoomer;
        product_zoom = new Product.Zoom('image', 'track', 'handle', 'zoom_in', 'zoom_out', 'track_hint');
    }
};



Product.OptionsPrice.prototype.reloadPriceLabels = function(productPriceIsKnown) {
    var priceFromLabel = '';
    var prodForm = $('product_addtocart_form');

    if (!productPriceIsKnown && typeof spConfig != "undefined") {
        priceFromLabel = spConfig.config.priceFromLabel;
    }

    var priceSpanId = 'configurable-price-from-' + this.productId;
    var duplicatePriceSpanId = priceSpanId + this.duplicateIdSuffix;

    if($(priceSpanId) && $(priceSpanId).select('span.configurable-price-from-label'))
        $(priceSpanId).select('span.configurable-price-from-label').each(function(label) {
        label.innerHTML = priceFromLabel;
    });

    if ($(duplicatePriceSpanId) && $(duplicatePriceSpanId).select('span.configurable-price-from-label')) {
        $(duplicatePriceSpanId).select('span.configurable-price-from-label').each(function(label) {
            label.innerHTML = priceFromLabel;
        });
    }
};



//SCP: Forces the 'next' element to have it's optionLabels reloaded too
Product.Config.prototype.configureElement = function(element) {
    this.reloadOptionLabels(element);
    if(element.value){
        this.state[element.config.id] = element.value;
        if(element.nextSetting){
            element.nextSetting.disabled = false;
            this.fillSelect(element.nextSetting);
            this.reloadOptionLabels(element.nextSetting);
            this.resetChildren(element.nextSetting);
        }
    }
    else {
        this.resetChildren(element);
    }
    this.reloadPrice();
};


//SCP: Changed logic to use absolute price ranges rather than price differentials
Product.Config.prototype.reloadOptionLabels = function(element){
    var selectedPrice;
    var childProducts = this.config.childProducts;

    //Don't update elements that have a selected option
    if(element.options[element.selectedIndex].config){
        return;
    }

    for(var i=0;i<element.options.length;i++){
        if(element.options[i].config){
            var cheapestPid = this.getProductIdOfCheapestProductInScope("finalPrice", element.options[i].config.allowedProducts);
            var mostExpensivePid = this.getProductIdOfMostExpensiveProductInScope("finalPrice", element.options[i].config.allowedProducts);
            var cheapestFinalPrice = childProducts[cheapestPid]["finalPrice"];
            var mostExpensiveFinalPrice = childProducts[mostExpensivePid]["finalPrice"];
            element.options[i].text = this.getOptionLabel(element.options[i].config, cheapestFinalPrice, mostExpensiveFinalPrice);
        }
    }
};

//SCP: Changed label formatting to show absolute price ranges rather than price differentials
Product.Config.prototype.getOptionLabel = function(option, lowPrice, highPrice){

    var str = option.label;

    if (!this.config.showPriceRangesInOptions) {
        return str;
    }

    var to = ' ' + this.config.rangeToLabel + ' ';
    var separator = ': ';

    lowPrices = this.getTaxPrices(lowPrice);
    highPrices = this.getTaxPrices(highPrice);

    if(lowPrice && highPrice){
        if (lowPrice != highPrice) {
            if (this.taxConfig.showBothPrices) {
                str+= separator + this.formatPrice(lowPrices[2], false) + ' (' + this.formatPrice(lowPrices[1], false) + ' ' + this.taxConfig.inclTaxTitle + ')';
                str+= to + this.formatPrice(highPrices[2], false) + ' (' + this.formatPrice(highPrices[1], false) + ' ' + this.taxConfig.inclTaxTitle + ')';
            } else {
                str+= separator + this.formatPrice(lowPrices[0], false);
                str+= to + this.formatPrice(highPrices[0], false);
            }
        } else {
            if (this.taxConfig.showBothPrices) {
                str+= separator + this.formatPrice(lowPrices[2], false) + ' (' + this.formatPrice(lowPrices[1], false) + ' ' + this.taxConfig.inclTaxTitle + ')';
            } else {
                str+= separator + this.formatPrice(lowPrices[0], false);
            }
        }
    }
    return str;
};


//SCP: Refactored price calculations into separate function
Product.Config.prototype.getTaxPrices = function(price) {
    var price = parseFloat(price);

    if (this.taxConfig.includeTax) {
        var tax = price / (100 + this.taxConfig.defaultTax) * this.taxConfig.defaultTax;
        var excl = price - tax;
        var incl = excl*(1+(this.taxConfig.currentTax/100));
    } else {
        var tax = price * (this.taxConfig.currentTax / 100);
        var excl = price;
        var incl = excl + tax;
    }

    if (this.taxConfig.showIncludeTax || this.taxConfig.showBothPrices) {
        price = incl;
    } else {
        price = excl;
    }

    return [price, incl, excl];
};

function AUA_ConfigurableAutoSelectFirstOption(no_first_selection) {
    localStorage.setItem('noAjaxaction', 'attivo');
    var ul = "";
    jQuery(".configurable-swatch-list li:not(.not-available) a").each(function(){
        //console.log("Analizzo un LI dalla lista: " + jQuery(this).parent().parent().attr("id"));
        if (no_first_selection) {
            //&& (jQuery(this).parent().parent().attr("id") != "configurable_swatch_eurofides_colore_shopper")
            if ((ul != jQuery(this).parent().parent().attr("id")) && (jQuery(this).parent().parent().attr("id") != "configurable_swatch_eurofides_colore_shopper") && (jQuery(this).parent().parent().attr("id") != "configurable_swatch_eurofides_colore_etichette") && (jQuery(this).parent().parent().attr("id") != "configurable_swatch_eurofides_colore_portabott") && (jQuery(this).parent().parent().attr("id") != "configurable_swatch_eurofides_colore_scatola")) {
                //console.log("Simulo il click su elemento sono in no_first_selection");
                jQuery(this).children("span").trigger("click");
                ul = jQuery(this).parent().parent().attr("id");
            }
        } else {
            if ((ul != jQuery(this).parent().parent().attr("id"))) {
                //console.log("Simulo il click su elemento non sono in no_first_selection");
                jQuery(this).children("span").trigger("click");
                ul = jQuery(this).parent().parent().attr("id");
            }
        }
    }).promise().done( function(){ 
        //console.log("Simulo il click sul giorno di consegna");
        jQuery(".btn-giorno-di-consegna").first().trigger("click");
        jQuery(".q_prodotto_personalizzato").first().trigger("click");
    } );
}

function ConfigurableGeneratePreview() {
    /**
     * Preparazione del preventivo del prodotto personalizzabile
     */
    var data_spedizione = jQuery("button.btn-giorno-di-consegna.btn-selected").html();
    jQuery("td.data_spedizione").html(data_spedizione);
    var qp_prodotto_selezionato = jQuery('.q_prodotto_personalizzato.qp_prodotto_selezionato').html();
    jQuery("td.pz_confezione").html(qp_prodotto_selezionato);
    var totale_netto = jQuery('.p_prodotto_personalizzato.qp_prodotto_selezionato').html();
    jQuery("td.totale_netto").html(totale_netto);
    var dettaglio = "";
    jQuery("dt.swatch-attr label").each(function () {
        var descrizione = jQuery(this).find(".select-label").html().split("-");
        if (descrizione[1]) {
            dettaglio = dettaglio + jQuery(this).html() +": "+ descrizione[1] +" <br />";
        } else {
            dettaglio = dettaglio + jQuery(this).html() +": "+ descrizione[0] +" <br />";
        }
        
    });
    jQuery("td.dettaglio_prodotto_selezionato").html(dettaglio);
    jQuery("div.il-tuo-preventivo").fadeIn(100);
}

//SCP: Forces price labels to be updated on load
//so that first select shows ranges from the start
document.observe("dom:loaded", function() {
    //Really only needs to be the first element that has configureElement set on it,
    //rather than all.
    $('product_addtocart_form').getElements().each(function(el) {
        if(el.type == 'select-one') {
            if(el.options && (el.options.length > 1)) {
                el.options[0].selected = true;
                if (spConfig) {
                    spConfig.reloadOptionLabels(el);
                }
            }
        }
    });
});

jQuery(document).ready(function() {
    if (jQuery(".btn-giorno-di-consegna").length) {
       // console.log("---------------------------------------");
       // console.log("- Prodotti Personalizzabili Javascript OK");
       // console.log("---------------------------------------");
        jQuery(".nascondi_option").hide();
        jQuery("div.il-tuo-preventivo").hide();
       // console.log("- Controllo quali sono le option della select e nascondo eventualmente i bottoni non necessari");
        var number_of_option = 0;
        jQuery(".btn-giorno-di-consegna").each(function () {
            /* controllo se i dati presenti in questo btn sono presenti anche nella rispettiva select */
            var option_value = jQuery(this).attr("data-option-value");
            var select_id = jQuery(this).attr("data-option-select-id");
             if (jQuery('#attribute'+select_id+' option[value="'+option_value+'"]').length == 0) {
                 jQuery(this).parent().remove();
             } else {
                 number_of_option++;
             }	
        });
       // console.log("Il numero di opzioni trovate: " + number_of_option);
        if (number_of_option == 1) {
            jQuery(".title_expedition_date_multiple_option").hide();
            jQuery(".price-variation").hide();
        }
        jQuery(".btn-giorno-di-consegna").click(function (e) {
            jQuery(".btn-selected").removeClass("btn-selected");
            e.preventDefault();
            /* recupero i dati dal campo data del bottone */
            var option_value = jQuery(this).attr("data-option-value");
            var select_id = jQuery(this).attr("data-option-select-id");
            jQuery('#attribute'+select_id+' option:eq('+option_value+')').prop('selected', true);
            jQuery('#attribute'+select_id+' option[value="'+option_value+'"]:not(:disabled)').attr('selected', 'selected');
            console.log("Valore della option giorni di consegna: " + jQuery('#attribute'+select_id).val());
            if (jQuery('#attribute'+select_id).val() != option_value) {
                 alert("Data di consegna stimata non selezionabile con questa combinazione di elementi.");
            } else {
                jQuery(this).addClass("btn-selected");
            }
        });
     }
     if (jQuery('td.q_prodotto_personalizzato').length) {
         jQuery('.price-box').hide();
         jQuery('.qty-label').hide();
         jQuery('#qty').hide();
         jQuery(document).on("click", "td.q_prodotto_personalizzato", function () {
         //jQuery('td.q_prodotto_personalizzato').click(function() {
             jQuery('.qp_prodotto_selezionato').removeClass('qp_prodotto_selezionato');
             jQuery(this).addClass('qp_prodotto_selezionato');
             var data_qty = jQuery(this).attr('data-qty');
             jQuery('input#qty').val(data_qty);
             jQuery(this).closest('td').next().addClass('qp_prodotto_selezionato');
             //console.log("Lancio la funzione ConfigurableGeneratePreview linea 635");
             ConfigurableGeneratePreview();
         });
     }

    jQuery("span.regular-price").on('DOMSubtreeModified',function(){
        console.group("Regular Price Change");
        console.info("Il contenuto dello span prezzo base è stato modificato");
        console.log("Il valore del regular price è: " + jQuery("span.regular-price span.price").text());
        var regular_price = jQuery("span.regular-price .price").text();
        /* posiziono il prezzo base nella prima riga della tabella */
        jQuery("td.p_prodotto_personalizzato[data-qty='1'] span.p_price").html(regular_price);
        jQuery("td.p_prodotto_personalizzato[data-qty='1'] span.p_price").parent().parent().show();
        /* metto lo stesso valore nella tabella del preventivo */
        console.log("Posiziono il regular parice: " + regular_price + " nella tabella del preventivo");
        jQuery("td.totale_netto").html(regular_price);
        console.groupEnd();
    });
    
    /* resetto eventuale prodotto semplice rimasto in cache */
    localStorage.setItem('oldchildProductId', null);
    /* avvio la funzione che autoseleziona la prima opzione disponibile di ogni attributo */
    AUA_ConfigurableAutoSelectFirstOption(false);
    jQuery("span.swatch-label").click(function() {
        if (jQuery(this).parent().parent().hasClass("not-available")) {
            //console.log("svuoto l'array dei selezionati");
            Product.ConfigurableSwatches.prototype._E.activeConfigurableOptions.splice(0,Product.ConfigurableSwatches.prototype._E.activeConfigurableOptions.length);
            jQuery("li.selected").each(function () {
                if ((jQuery(this).parent().attr("id") != 'configurable_swatch_eurofides_colore_shopper') && (jQuery(this).parent().attr("id") != 'configurable_swatch_eurofides_colore_etichette') && (jQuery(this).parent().attr("id") != 'configurable_swatch_eurofides_colore_portabott')  && (jQuery(this).parent().attr("id") != 'configurable_swatch_eurofides_colore_scatola')) {
                    //console.log("Rimuovo la classe selected da questo LI: " + jQuery(this).attr('id'));
                    //console.log("Svuoto anche la select del relativo attributo");
                    jQuery(this).parent().parent().find("select").val("");
                    jQuery(this).removeClass("selected");
                }
            });
            //AUA_ConfigurableAutoSelectFirstOption(false);
            jQuery("#modale-loadOptions .modal-header").addClass("hide");
            jQuery("#modale-loadOptions .modal-footer").addClass("hide");
            jQuery("#modale-loadOptions div.modal-body div.modal-body-dynamic-content").html('<center><img class="loading-options" style="margin-left:10px" src="'+_ajaxloaderimg+'" /></center>');
            jQuery("#modale-loadOptions").modal('show');
            setTimeout(
                function() 
                {
                   // console.log("Start AUA_ConfigurableAutoSelectFirstOption function after 3 seconds");
                    AUA_ConfigurableAutoSelectFirstOption(true);
                    jQuery("#modale-loadOptions").modal('hide');
                    jQuery("#modale-loadOptions div.modal-body div.modal-body-dynamic-content").html('');
                }, 1000);
        }
    });
});